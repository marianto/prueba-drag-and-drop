function allowDrop(ev) {
    ev.preventDefault();
}
function dragStart(ev) {
    ev.dataTransfer.setData("text/plain", ev.target.id);
}
function dropIt(ev) {
    ev.preventDefault();
    let sourceId = ev.dataTransfer.getData("text/plain");
    let sourceIdEl = document.getElementById(sourceId);
    let sourceIdParentEl = sourceIdEl.parentElement;
    let targetEl = document.getElementById(ev.target.id)
    let targetParentEl = targetEl.parentElement;


    if (targetParentEl.id !== sourceIdParentEl.id) {
        if (targetEl.className === sourceIdEl.className) {

            targetParentEl.appendChild(sourceIdEl);

        } else {

            targetEl.appendChild(sourceIdEl);

        }

    } else {

        let holder = targetEl;

        let holderText = holder.textContent;

        targetEl.textContent = sourceIdEl.textContent;
        sourceIdEl.textContent = holderText;
        holderText = '';
    }

}

const droppableElements = document.querySelectorAll('.board-líst');
const draggableElements = document.querySelectorAll('.card');
droppableElements.forEach(elem => {
    elem.addEventListener("drop", drop);
})

function drop(event) {
    event.preventDefault();
    event.dataTransfer.setData(event.target.style.color);

}

draggableElements.forEach(elem => {
    elem.addEventListener('dragend', dragEnd);
})

function dragEnd(event) {
    const droppableElementsData = event.dataTransfer.getData();
    event.target.style.backgroundColor = droppableElements;
}
